package controls

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"lamethode.xyz/geheimnis/models"
)

func AuthUser(username, password string) bool {
	var user models.User
	err := models.DB.Where(models.User{Login: username, Password: password}).First(&user)
	return err.Error == nil
}

func GetAllSecret(c *gin.Context) {
	var secrets []models.Secret
	models.DB.Find(&secrets)

	c.JSON(http.StatusOK, gin.H{"secrets": secrets})
}

func GetSecret(c *gin.Context) {
	var secret models.Secret
	if err := models.DB.Where("id = ?", c.Param("id")).First(&secret).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "secret not found"})
		return
	}
	if uint(time.Now().Unix()) > secret.EoL {
		c.JSON(http.StatusBadRequest, gin.H{"error": "secret is outdated"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"secret": secret.Secret})
}

func MakeSecret(c *gin.Context) {
	now := uint(time.Now().Unix())
	secret := models.Secret{
		Secret: models.RSBMISU(16),
		ID:     uuid.New(),
		EoL:    uint(now + 3600),
	}
	models.DB.Create(&secret)

	c.JSON(http.StatusOK, gin.H{
		"uuid":   secret.ID,
		"secret": secret.Secret,
		"ttl":    3600,
	})
}

func MakeSecretOpt(c *gin.Context) {
	ttl, _ := strconv.ParseUint(c.DefaultQuery("ttl", "3600"), 10, 64)
	len, _ := strconv.ParseInt(c.DefaultQuery("length", "16"), 10, 64)
	now := uint64(time.Now().Unix())
	eol := now + ttl
	secret := models.Secret{
		Secret: models.RSBMISU(int(len)),
		ID:     uuid.New(),
		EoL:    uint(now + ttl),
	}
	models.DB.Create(&secret)

	c.JSON(http.StatusOK, gin.H{
		"uuid":   secret.ID,
		"secret": secret.Secret,
		"ttl":    eol,
	})
}

func PostSecret(c *gin.Context) {
	var input models.Writing
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	now := uint(time.Now().Unix())
	secret := models.Secret{
		Secret: input.Secret,
		ID:     uuid.New(),
		EoL:    uint(now + input.TTL),
	}
	models.DB.Create(&secret)

	c.JSON(http.StatusOK, gin.H{"uuid": secret.ID})
}

func DeleteSecret(c *gin.Context) {
	var secret models.Secret
	if err := models.DB.Where("id = ?", c.Param("id")).Delete(&secret).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "secret not found"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"Status": "Deleted"})
}

func CleanEol(c *gin.Context) {
	var secrets []models.Secret
	models.DB.Where("EoL < ?", uint(time.Now().Unix())).Delete(&secrets)

	c.JSON(http.StatusOK, gin.H{"data": secrets})
}
