package main

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"strings"

	"lamethode.xyz/geheimnis/controls"
	"lamethode.xyz/geheimnis/models"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func home(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"Hello": "world"})
}

func Config() (config models.Config, err error) {
	viper.AddConfigPath(".")
	viper.SetConfigName("geheimnis")
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}

// register users:
// $ sqlite3 hmts.db "insert into users (login, password) values ('user','pass');"
func BasicAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		conf, _ := Config()
		var p models.User
		p.Login = conf.User
		p.Password = conf.Pass

		auth := strings.SplitN(c.Request.Header.Get("Authorization"), " ", 2)

		if len(auth) != 2 || auth[0] != "Basic" {
			respondWithError(401, "Unauthorized", c)
			return
		}
		payload, _ := base64.StdEncoding.DecodeString(auth[1])
		pair := strings.SplitN(string(payload), ":", 2)

		if p.Login == pair[0] && p.Password == pair[1] {
			c.Next()
			return
		}

		if len(pair) != 2 || !controls.AuthUser(pair[0], pair[1]) {
			respondWithError(401, "Unauthorized", c)
			return
		}

		c.Next()
	}
}

func respondWithError(code int, message string, c *gin.Context) {
	resp := map[string]string{"error": message}

	c.JSON(code, resp)
	c.Abort()
}

func main() {

	conf, _ := Config()
	models.ConnectDB()
	router := gin.Default()

	unsecured := router.Group("/")
	{
		unsecured.GET("/", home)
		unsecured.GET("/secret/:id", controls.GetSecret)
		unsecured.GET("/make", controls.MakeSecret)
		unsecured.POST("/make", controls.MakeSecretOpt)
		unsecured.POST("/secret", controls.PostSecret)
	}

	authorized := router.Group("/", BasicAuth())
	{
		authorized.GET("/secrets", controls.GetAllSecret)
		authorized.DELETE("/secret/:id", controls.DeleteSecret)
		authorized.DELETE("/clean", controls.CleanEol)

	}
	router.Run(fmt.Sprintf("%s:%s", conf.Ip, conf.Port))
}
