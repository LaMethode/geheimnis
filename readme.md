# Geheimnis

a simple secret sharing web service written in go

## Foreword

This is a little side project.
Don't expect too much from this.

## The calls

(yeah, I'm too lazy to make a swagger)

---
`POST /secret`

```json
{
    "secret":"mySecret",
    "ttl": 3600
}
```

* secret is the secret you want to share
* ttl is the lifespan of your secret (in seconds)

You will got this to share your secret:

```json
{
  "uuid": "cbed2d6f-b221-489f-be5f-4994db9915f7"
}
```

---
`GET /secret/:uuid`

you'll get

```json
{
  "secret": "mySecret"
}
```

if the `TTL` is reached, the answer will be:

```json
{
    "error": "secret is outdated"
}
```

---
`GET /make`
Will create you a new random secret with a 1h (3600s) TTL.

```json
{
  "secret": "7U6sebWeBhPyQjST",
  "ttl": 3600,
  "uuid": "58e3dfb5-9851-4130-b425-53640213142e"
}
```

---
`POST /make?length=1234&ttl=1234`
Will create you a new `length` caracters random secret with a `ttl`s TTL.  
Default values are:

* length = 16
* ttl = 3600s

```json
{
  "secret": "7U6sebWeBhPyQjST",
  "ttl": 1234,
  "uuid": "58e3dfb5-9851-4130-b425-53640213142e"
}
```

---

## Admin commands

`DELETE /secret/:uuid`
Delete matching secret

`DELETE /clean`
Delete all outdated secrets

`GET /secrets`
List all secrets

> All theses commands need authentification  
> Set your env with  
> DEFAULT_USER=yourUser  
> DEFAULT_PASS=yourPassword
---
