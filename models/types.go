package models

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Config struct {
	Ip   string `mapstructure:"ADDRESS"`
	Port string `mapstructure:"PORT"`
	User string `mapstructure:"DEFAULT_USER"`
	Pass string `mapstructure:"DEFAULT_PASS"`
}

type User struct {
	Login    string `gorm:"column:login;primary_key" json:"login"`
	Password string `gorm:"column:password" json:"password"`
}

type Writing struct {
	Secret string `form:"secret" json:"secret" binding:"required"`
	TTL    uint   `form:"ttl" json:"ttl" binding:"required"`
}

type Secret struct {
	ID     uuid.UUID `json:"id"     gorm:"column:id;primary_key"`
	Secret string    `json:"secret" gorm:"column:secret"`
	EoL    uint      `json:"eol"    gorm:"column:eol"`
}

type repo struct {
	BDD *gorm.DB
}

type Vault interface {
	Get(ID uuid.UUID) (*Secret, error)
	Create(ID uuid.UUID, Secret string, EoL uint) error
}

func (s *repo) Create(scribble Writing) error {

	now := uint(time.Now().Unix())
	secret := &Secret{
		Secret: scribble.Secret,
		ID:     uuid.New(),
		EoL:    uint(now + scribble.TTL),
	}
	return s.BDD.Create(secret).Error
}
